import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type UserDocument = User & Document;

@Schema()
export class User {
    @Prop()
    fullname: string;
    @Prop({type: String, required: true, trim: true, select: false})
    email: string;
    @Prop()
    password: string;
    @Prop({ type: Date, required: true, default: Date.now })
    created_at: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);