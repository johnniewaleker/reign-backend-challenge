// import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { map } from 'rxjs';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { News, NewsDocument } from './schema/news.schema';
import { User, UserDocument } from '../users/schema/users.schema';
import { hash } from "bcrypt"

@Injectable()
export class NewsService {

  constructor(
    private httpService: HttpService,
    @InjectModel(News.name) private newsSchema: Model<NewsDocument>,
    @InjectModel(User.name) private userSchema: Model<UserDocument>,
  ) { }

  async getNews() {
    let news = this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      {
        headers: {
          'Accept': 'application/json'
        }
      }).pipe(
        map(response => {
          return response.data
        }),
      );

    // parse the data and convert to simple array of objects
    let data = [];
    await news.forEach(element => {
      element.hits.forEach(elem => {
        data.push(elem);
      });
    });

    data.forEach(async (news) => {
      const newsFound = await this.newsSchema.find({ "parent_id": news.parent_id });
      if (newsFound.length == 0) {
        this.newsSchema.create({
          "created_at": news.created_at,
          "title": news.title,
          "url": news.url,
          "author": news.author,
          "points": news.points,
          "story_text": news.story_text,
          "comment_text": news.comment_text,
          "num_comments": news.num_comments,
          "story_id": news.story_id,
          "story_title": news.story_title,
          "story_url": news.story_url,
          "parent_id": news.parent_id,
          "created_at_i": news.created_at_i,
          "_tags": news._tags,
          "objectID": news.objectID,
        });
      }
    });

    const userFound = await this.userSchema.find({ "email": "john.doe@example.com" });
    if (userFound.length == 0) {
      this.userSchema.create({
        fullname: "John Doe",
        email: "john.doe@example.com",
        password: await hash("123456", 10)
      });
    }

    return data
  }

  async listNews(req) {
    // verify pagination and set default value
    let pagination = {
      page: req['page'] ? parseInt(req['page']) : 1,
      perPage: req['perPage'] ? parseInt(req['perPage']) : 5,
    }

    // start the mongoose agregateQuery and query
    let agregateQuery = [];
    let matchQuery = {}
    // NOTE: Example of the structure of the agregateQuery array
    // db.test.aggregate([
    //   {$addFields: {  "month" : {$month: '$created_at'}}},
    //   {$match: matchQuery}
    // ]);

    // get the month number of req and created_at field

    if (req.month != undefined && req.month != "") {
      let month = this.monthParse(req.month);
      if (month != undefined) {
        agregateQuery.push({ "$addFields": { "month": { "$month": '$created_at' } } });
        matchQuery['month'] = month;
      }
    }

    // Search for the author with the similarities found based on the entered word
    if (req.author != undefined && req.author != "") {
      matchQuery['author'] = { "$regex": req.author, "$options": 'i' };
    }

    // search for one or more tags
    if (req.tags != undefined && req.tags != "") {
      let tags = req.tags.split(',');
      matchQuery['_tags'] = { "$in": tags };
    }

    // Search for the title with the similarities found based on the entered word
    if (req.title != undefined && req.title != "") {
      matchQuery['story_title'] = { "$regex": req.title, "$options": 'i' }
    }
    // added the matchQuery to agregateQuery arary to send to mongoose
    agregateQuery.push({ "$match": matchQuery });

    const startIndex = (pagination.page - 1) * pagination.perPage;
    const endIndex = pagination.page * pagination.perPage;

    const results = {}

    // make the agregateQuery and paginate the results from the db
    let news = await this.newsSchema.aggregate(agregateQuery).limit(endIndex).skip(startIndex);

    if (news.length > 0) {
      results['next'] = {
        page: pagination.page + 1,
        limit: pagination.perPage
      }
    }

    if (startIndex > 0) {
      results['previous'] = {
        page: pagination.page - 1,
        limit: pagination.perPage
      }
    }

    results["results"] = news;
    return results;
  }

  async deleteNews(newsID: string) {
    const deletedNews = await this.newsSchema.findByIdAndDelete(newsID);
    return deletedNews;
  }

  monthParse(month) {
    const monthNames = {
      "january": 1,
      "february": 2,
      "march": 3,
      "april": 4,
      "may": 5,
      "june": 6,
      "july": 7,
      "august": 8,
      "september": 9,
      "october": 10,
      "november": 11,
      "december": 12
    };
    return monthNames[month.toLowerCase()];
  }
}
