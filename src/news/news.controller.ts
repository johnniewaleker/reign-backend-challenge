import { Controller, Get, Query, Delete, Param, NotFoundException, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags, ApiParam, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { NewsService } from './news.service';

@ApiBearerAuth()
@ApiTags('News')
@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @Get('/get')
  @ApiOperation({ summary: 'This endpoint is use to populate db' })
  @ApiResponse({ status: 201, description: 'Database successfully populated, and user created too!.'})
  @ApiResponse({ status: 400, description: 'There was an error populating the database.'})

  async getNews() {
    const news = await this.newsService.getNews();
    if (!news) throw new NotFoundException( {
      status: "fail",
      message: "There was an error populating the database",
    });
    return {
      status: "success",
      message: "Database successfully populated, and user created too!",
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  @ApiOperation({ summary: 'This endpoint is use to get the news' })
  @ApiParam({
    name: 'month',
    type: 'string',
    description: 'search by month word, eg.. "may, june, july"',
    required: false,
  })
  @ApiParam({
    name: 'title',
    type: 'string',
    description: 'search by news title',
    required: false,
  })
  @ApiParam({
    name: 'tags',
    type: 'string',
    description: 'search by tags, it could be by severals tags sepatarated by "," e.g. "comment,story_31466885,author_high_byte"',
    required: false,
  })
  @ApiParam({
    name: 'author',
    type: 'string',
    description: 'search by news author',
    required: false,
  })
  @ApiParam({
    name: 'perPage',
    type: 'number',
    description: 'by default the resposponse return 5 element per page, but can change quantity of elements if you want',
    required: false,
  })
  @ApiParam({
    name: 'page',
    type: 'number',
    description: 'the number of the page that you want to see',
    required: false,
  })
  async listNews(@Query() query: any) {
    const news = await this.newsService.listNews(query);
    if (!news) throw new NotFoundException( {
      status: "fail",
      message: "the news not foundt",
    });
    return {
      status: "success",
      data: news,
    }
  }

  @UseGuards(JwtAuthGuard)
  @ApiParam({
    name: 'newsID', 
    required: true, 
    description: 'This endpoint is use to delete a news', 
    type: 'string'
  })
  @Delete(':newsID')
  async deleteNews(@Param('newsID') newsID) {
    const news = await this.newsService.deleteNews(newsID);
    if (!news) throw new NotFoundException( {
      status: "fail",
      message: "the news doesn't exist",
    });

    return {
      status: "success",
      message: "News deleted succesfully",
      news_deleted: news,
    }
  }
}
