import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { MongooseModule } from '@nestjs/mongoose';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { News, NewsSchema } from "./schema/news.schema";
import { User, UserSchema } from "../users/schema/users.schema";

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([
      { name: News.name, schema: NewsSchema },
      { name: User.name, schema: UserSchema }
    ])
  ],
  controllers: [NewsController],
  providers: [NewsService],
})
export class NewsModule {}
