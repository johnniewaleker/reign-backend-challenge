import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { LoginAuthDto } from './dto/login-auth.dto';
import { User, UserDocument } from '../users/schema/users.schema';
import { compare, hash } from "bcrypt"
import { JwtService } from '@nestjs/jwt';


@Injectable()
export class AuthService {

  constructor(
    @InjectModel(User.name) private userSchema: Model<UserDocument>,
    private jwtAuthService:JwtService
  ) { }

  async login(userObjectLogin: LoginAuthDto) {
    const { email ,password } = userObjectLogin;
    const findUser = await this.userSchema.findOne({email});
    
    if  (!findUser) throw new HttpException("USER_NOT_FOUND", 404);
    
    const checkPassword = await compare(password, findUser['password']);
    if (!checkPassword) throw new HttpException("PASSWORD_INCORRECT", 403);

    const payload = { id: findUser._id, fullname: findUser.fullname}
    const token = this.jwtAuthService.sign(payload)

    const data = {
      user: findUser,
      token,
    }

    return data;
  }
}
