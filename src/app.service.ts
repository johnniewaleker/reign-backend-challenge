import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello Reign Team!, this is the api challenge for the backend position.';
  }
}
