<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

<h1 align="center">
  Reign Backend Challenge
</h1>
<p align="">
Description:
<br>
The purpose of this project is to solve the challenge that Reign's recruitment process requires me
to solve, in order to showcase my skills for the Backend Developer position.</p>

---
### Challenge
Build a small API to test your knowledge of Back End Development and related technologies.
The server, once an hour, should connect to the API (refer to the below url) which shows
recently posted articles about Node.js on Hacker News. It should insert the data from the
API into a database and also define a REST API which the client (e.g. Postman) will be used
to retrieve the data.

Hacker News URL: https://hn.algolia.com/api/v1/search_by_date?query=nodejs

The service should return paginated results with a maximum of 5 items and should be able
to be filtered by author, _tags, title and also be searchable by month word (e.g. september)
using the “created_at” field. Also, this should permit the user to remove items and these
ones should not reappear when the app is restarted.

In order to access the endpoints, an authorization parameter with a JWT must be sent in
the header.

### Considerations
- Node.js version active LTS
- The server component should be coded in TypeScript.
- At least 30% test coverage (statements) for the server component.
- The whole project has to be uploaded to GitLab.
- The artifacts (server API) must be Dockerized.
- To start the project there should be a docker-compose that uses both images and
the database image.

### Stack
You must use the following technologies to build the app:
- Active LTS version of Node.js + NestJS.
- Database: MongoDB or PostgreSQL.
- ORM: Mongoose or TypeORM.
- API Doc: Swagger, should be exposed at /api/docs.
- Docker

## Instructions
---
### Step 1: Installation
You can download the project using git clone.
```sh
git clone https://gitlab.com/johnniewaleker/reign-backend-challenge.git
```
Or also downloading it directly from this link:
```sh
https://gitlab.com/johnniewaleker/reign-backend-challenge
```
### Step 2: Docker and Docker-Compose Installation
Make sure you have docker and docker-compose installed, if you do not have them installed you can 
download them using the following links, the installation instructions are right there:

- [Docker](https://docs.docker.com/desktop/windows/install/) - Windows - incluye docker-compose
- [Docker](https://docs.docker.com/desktop/mac/install/) - Mac - incluye docker-compose
- [Docker](https://docs.docker.com/desktop/linux/install/) - Linux - no incluye docker

to install docker-compose on linux, this has to be done manually, that is solved
installing it manually with the following command:
```sh
$ sudo apt-get update
$ sudo apt-get install docker-compose-plugin
```

### Step 3: Run Docker and Docker-Compose
In order to use the API and MongoDB, you must run docker, once docker is running
on your computer you must position yourself in the root of the project and run 
docker compose to mount the project with the following command:

```sh
$ docker-compose up dev
```
what this will do is mount the project infrastructure in docker as well as execute
the API and the MongoDB database.

the routes are as follows:
API:
```
http://localhost:3000
```
MongoDB:
```
mongodb://localhost:27017/challengedb
```

### Step 4: Populate the database
To populate the database, a specific enpoint must be called, which will be in charge of 
consulting an external enpoint and storing its information within MongoDB.

```sh
GET http://localhost:3000/news/get
```

### Step 5: Login Credentials and Documentation
When populating the database, a default user will be created to be able to consume the 
news enpoints, these are the user's credentials.
JSON:
```Json
{
    "email": "john.doe@example.com",
    "password": "123456"
}
```

You will find the documentation about the endpoints in the following link
```sh
http://localhost:3000/api/docs/
```

## License
MIT